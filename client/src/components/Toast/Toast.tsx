import * as React from 'react'
import { Layer, LayerProps } from 'grommet'

interface IProps extends LayerProps {
  children: React.ReactNode
  duration: number
  onClose: () => void
}

export class Toast extends React.Component<IProps> {
  timeoutAutoHide: any
  componentDidMount() {
    this.startAutoHideTimeout()
  }

  componentWillUnmount() {
    const { timeoutAutoHide } = this
    if (timeoutAutoHide) {
      clearTimeout(this.timeoutAutoHide)
    }
  }

  startAutoHideTimeout() {
    const { duration, onClose } = this.props
    if (duration) {
      this.timeoutAutoHide = setTimeout(() => {
        onClose()
      }, duration * 1000)
    }
  }

  render() {
    const { children, modal = false, position, full, ...rest } = this.props
    return (
      <Layer
        onClick={rest.onClose}
        position={position || 'top'}
        full={full}
        modal={modal}
        margin="none"
        responsive
        plain={modal ? false : true}
        {...rest}
      >
        {children}
      </Layer>
    )
  }
}
