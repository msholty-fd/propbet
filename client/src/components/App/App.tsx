import * as React from 'react'
import { ApolloProvider } from 'react-apollo'
import { Grommet } from 'grommet'
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'
import { Router } from '../Router'
import { UserProvider } from '../../pages/Login/UserContext'
import { client } from '../../utils/apollo'

export function App() {
  return (
    <Grommet full>
      <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <UserProvider>
            <Router />
          </UserProvider>
        </ApolloHooksProvider>
      </ApolloProvider>
    </Grommet>
  )
}

export default App
