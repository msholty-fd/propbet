import * as React from 'react'
import useReactRouter from 'use-react-router'
import { Box, Form, FormField, TextInput, Button } from 'grommet'
import { UserContext, ISignUpForm } from './UserContext'
import { Toast } from '../../components/Toast'

interface ISignUpFormInputs extends ISignUpForm {
  confirmPassword?: string
}

export function SignupForm() {
  const [formError, setFormError] = React.useState<string | null>(null)
  const { signUp } = React.useContext(UserContext)
  const { history } = useReactRouter()

  async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()

    const data = new FormData(event.target as HTMLFormElement)
    const allData = (Object.fromEntries(data.entries()) as unknown) as ISignUpForm

    try {
      await signUp(allData)
      history.push('/')
    } catch (e) {
      setFormError(e.toString())
    }
  }

  function validateConfirmPassword(
    confirmPassword: string | undefined,
    formData: ISignUpFormInputs
  ) {
    const { password } = formData

    if (!confirmPassword) {
      return 'You have to type something!'
    }

    if (password !== confirmPassword) {
      return 'Password and password confirmation must match'
    }
  }

  return (
    <Box align="center" fill justify="center">
      <Form onSubmit={handleSubmit} style={{ margin: 10 }}>
        <FormField component={TextInput} name="email" id="email" label="Email" />
        <FormField
          component={TextInput}
          name="password"
          id="password"
          type="password"
          label="Password"
        />
        <FormField
          component={TextInput}
          name="confirmPassword"
          id="confirmPassword"
          type="confirmPassword"
          label="Confirm Password"
          validate={validateConfirmPassword}
        />
        <Button type="submit" primary label="Sign Up" style={{ width: '100%' }} />
        {formError && (
          <Toast
            margin="small"
            position="top"
            responsive={false}
            onClose={() => setFormError(null)}
            onEsc={() => setFormError(null)}
            duration={5}
          >
            {formError}
          </Toast>
        )}
      </Form>
    </Box>
  )
}
