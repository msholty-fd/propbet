import React from 'react'
import useReactRouter from 'use-react-router'
import { Button, Box, TextInput, Form, FormField } from 'grommet'
import { UserContext } from './UserContext'
import { Toast } from '../../components/Toast'

export function LoginForm() {
  const [formError, setFormError] = React.useState<string | null>(null)
  const { logIn } = React.useContext(UserContext)
  const { history } = useReactRouter()

  async function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault()
    const data = new FormData(event.target as HTMLFormElement)
    const username = data.get('username') as string
    const password = data.get('password') as string
    try {
      await logIn(username, password)
      history.push('/')
    } catch (e) {
      setFormError(e.toString())
    }
  }

  return (
    <Box align="center" fill justify="center">
      <Form onSubmit={handleSubmit}>
        <FormField component={TextInput} name="username" id="username" label="Email" required />
        <FormField
          component={TextInput}
          name="password"
          id="password"
          type="password"
          label="Password"
          required
        />
        <Button type="submit" primary label="Login" style={{ width: '100%' }} />
        {formError && (
          <Toast
            margin="small"
            position="top"
            responsive={false}
            onClose={() => setFormError(null)}
            onEsc={() => setFormError(null)}
            duration={5}
          >
            {formError}
          </Toast>
        )}
      </Form>
    </Box>
  )
}
